/*
 * (c) 2021 by Eric Dubuis - BFH BTX8041, Programming 1
 */
/**
 * A module for a JavaFX Hello World app.
 */
module btx.prog.one.hellomvn {
    requires transitive javafx.controls;
    exports btx.prog.one.hellomvn;
}
