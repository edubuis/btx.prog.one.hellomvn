# Problem with JavaFX and Apple M1 (Silicon) Chip and Eclipse

> Update: After several trials I figured out a workaround to get this
> JavaFX / Apple M1 (Silicon) / Eclipse problem: When installing Eclipse 2021-09,
> use JRE 17.0.0, https://download.eclipse.org/justj/jres/17/updates/release/17.0.0,
> in the installer form for the Java 11+ VM specification.
>
> With this JRE at hand, the JavaFX application can be started successfully
> from within Eclipse. Changing the project's JRE back to another Java 17 JRE,
> however, still yields the problem described below.


I report here another problem I have encountered when running a JavaFX 17
application from within Eclipse on a brand-new Apple M1 (Silicon) based
laptop.

Executing the simple Hello World app from within Eclipse (right-click
class App / Run As / Java Application) yields the following stack trace:

```console
Loading library prism_es2 from resource failed: java.lang.UnsatisfiedLinkError: /Users/due1/.openjfx/cache/17.0.0.1/libprism_es2.dylib: dlopen(/Users/due1/.openjfx/cache/17.0.0.1/libprism_es2.dylib, 1): no suitable image found.  Did find:
	/Users/due1/.openjfx/cache/17.0.0.1/libprism_es2.dylib: mach-o, but wrong architecture
	/Users/due1/.openjfx/cache/17.0.0.1/libprism_es2.dylib: mach-o, but wrong architecture
...
```

Inspecting the user's cache for JavaFX shared libraries yields:

```console
due1@host 17.0.0.1 % file * 
libglass.dylib:       Mach-O 64-bit dynamically linked shared library arm64
libjavafx_font.dylib: Mach-O 64-bit dynamically linked shared library arm64
libprism_es2.dylib:   Mach-O 64-bit dynamically linked shared library x86_64
libprism_sw.dylib:    Mach-O 64-bit dynamically linked shared library x86_64
```
Clearly, files `libprism_es2.dylib` and `libprism_sw.dylib` are not one for
the required M1 chip architecture.

However, if executed via Eclipse's Maven command, the application can be executed:

```console
...
[INFO] --- javafx-maven-plugin:0.0.6:run (default-cli) @ hellomvn ---
[INFO] Toolchain in javafx-maven-plugin null
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
```

Having imported the project into IntelliJ 2021.2.x, the simple application
can be executed. Then, the user's cache for JavaFX shared libraries looks like:

```console
due1@host 17.0.0.1 % file *
libglass.dylib:       Mach-O 64-bit dynamically linked shared library arm64
libjavafx_font.dylib: Mach-O 64-bit dynamically linked shared library arm64
libprism_es2.dylib:   Mach-O 64-bit dynamically linked shared library arm64
```

And it seems that file `libprism_sw.dylib` (see Eclipse case) is not used.

Any clue?

----

# Problem with JavaFX 17 and Maven

> **Important Note:** This problem is solved, see
> [IntelliJ doesn't load javafx packages...](https://stackoverflow.com/questions/69116905/intellij-doesnt-load-javafx-packages-from-maven-dependencies-javafx-17), by
> using Java 17 and JavaFX 17.0.0.1.

This demo program illustrates a problem I have found when
using Maven to compile the application using Java JDK 17
and JavaFX 17. I'm using Maven 3.8.1.

If I compile the application in a terminal
with Maven, then these errors (excerpt) are produced:

```console
% mvn clean compile
...
[INFO] --- maven-compiler-plugin:3.8.1:compile (default-compile) @ hellomvn ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 3 source files to /Users/due1/workspace-tmp/btx.prog.one.hellomvn/target/classes
[INFO] -------------------------------------------------------------
[ERROR] COMPILATION ERROR : 
[INFO] -------------------------------------------------------------
[ERROR] /path_to/btx.prog.one.hellomvn/src/main/java/btx/prog/one/hellomvn/App.java:[6,26] package javafx.application does not exist
[ERROR] /path_to/btx.prog.one.hellomvn/src/main/java/btx/prog/one/hellomvn/App.java:[7,20] package javafx.scene does not exist
...
```

However, if imported
into Eclipse 2021-09 (with Java 17 Support Feature) or
IntelliJ, then the application compiles and can be
executed.

If I change the JavaFX version in the POM from `17` to `16`,
then Maven compiles the application:

```
		<java.version>17</java.version>
		<javafx.version>16</javafx.version>
```

Using `mvn -U clean compile` does not help either. I noticed
when using Maven's (big-) `-X` option that with JavaFX 17, the
JAR's of JavaFX are *not* on the classpath, but on the
module-path only, whereas when specifying JavaFX 16,
the JAR's of JavaFX 16 *are* on both, the classpath
and module-path.


Any clue?
